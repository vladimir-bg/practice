import React from "react";
import {Link} from "react-router-dom"
import "./css/home.css"
import "./css/footer.css"
import "./css/usage.css"
import "./css/section.css"
import Android from "./img/google-play-badge.svg"
import Apple from "./img/app-store-badge.svg"
import Video from "./img/demo-screen.mp4"

export class Home extends React.Component {
    render() {
        return <div className="home">
            <div className="homePage">
                <div className="page">
                    <div className="row">
                        <div className="text-element">
                            <div className="texts">
                                <h2 className="h1-text">Start tracking your money</h2>
                                <p className="appParagraph">Download the application for easy mobile use</p>
                                <div className="icons">
                                    <a className="a-item" href="https://play.google.com/store/apps">
                                        <img className="a-item-image" src={Android} alt="Android"/>
                                    </a>
                                    <a className="a-item" href="https://www.apple.com/app-store">
                                        <img className="a-item-image" src={Apple} alt="Apple"/>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div className="text-element">
                            <div className="measured-device">
                                <svg className="shape-1" viewBox="0 0 240.83 240.83" xmlns="http://www.w3.org/2000/svg">
                                    <rect x="-32.54" y="78.39" width="305.92" height="84.05" rx="42.03"
                                          transform="translate(120.42 -49.88) rotate(45)"/>
                                    <rect x="-32.54" y="78.39" width="305.92" height="84.05" rx="42.03"
                                          transform="translate(-49.88 120.42) rotate(-45)"/>
                                </svg>
                                <svg className="shape-2" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="50" cy="50" r="50"/>
                                </svg>
                                <div className="device-wrapper">
                                    <div className="device">
                                        <div className="screen">
                                            <video className="video" muted="muted" autoPlay loop>
                                                <source src={Video} type="video/mp4"/>
                                            </video>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="usage">
                <div className="usage-box">
                    <div className="usage-container">
                        <div className="usage-text">
                            "An intuitive solution to a common problem that
                            we all face, wrapped up in a single app!"
                        </div>
                        {/*<img src={Logo} alt="..."/>*/}
                    </div>
                </div>
            </div>
            <section className="section" id="download">
                <div className="section-container">
                    <h2 className="section-text">Get the app now!</h2>
                    <div className="section-div">
                        <a className="a-item" href="https://play.google.com/store/apps">
                            <img className="a-item-image" src={Android} alt="Android"/>
                        </a>
                        <a className="a-item" href="https://www.apple.com/app-store">
                            <img className="a-item-image" src={Apple} alt="Apple"/>
                        </a>
                    </div>
                </div>
            </section>
            <footer className="footer">
                <div className="footer-container">
                    <div className="container-div">
                        <div className="privacy">&copy; Your Website 2021. All Rights Reserved.</div>
                        <Link className="link">Privacy</Link>
                        <span className="terms">&middot;</span>
                        <Link className="link">Terms</Link>
                        <span className="faq">&middot;</span>
                        <Link className="link">FAQ</Link>
                    </div>
                </div>
            </footer>
        </div>
    }
}