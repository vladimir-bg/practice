import React from "react";
import "./css/navBar.css"
import axios from "axios";
import {ComponentController} from "./components/ComponentController";
import {Link} from "react-router-dom";

export class NavBar extends React.Component {
    constructor(props){
        super(props)
        this.state = {value: null}
    }

    componentDidMount() {
        axios.get('http://localhost:8080/api/token', {withCredentials: true})
            .then(response => {
                const value = response.data;
                this.setState({ value });
            }).catch(errors => {
                const value = null;
                this.setState({ value });
                console.log(errors);
            })
    }

    render() {
        const unauthorizedComponents = ['register', 'login', 'home'];
        const authorizedComponents = ['user', 'logout', 'home'];

        if(this.state.value !== null) return <div className="nav-bar">
            <div className="nav-bar-right">
                <>
                    {
                        authorizedComponents.map((item) => {
                            return ComponentController(item);
                        })
                    }
                </>
            </div>
        </div>
        else return <div className="nav-bar">
            <div className="nav-bar-right">
                <>
                    {
                        authorizedComponents.map((item) => {
                            return ComponentController(item);
                        })
                    }
                </>
                <Link to="/register"> Register </Link>
                <Link to="/login">Login</Link>
                <Link to="/"> Home </Link>
            </div>
        </div>
    }
}