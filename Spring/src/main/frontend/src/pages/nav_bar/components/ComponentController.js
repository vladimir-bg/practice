import {Link} from "react-router-dom";

export function ComponentController(props) {
    switch (props) {
        case'home':
            return <Link to="/"> Home </Link>;
        case 'login':
            return <Link to="/login">Login</Link>;
        case 'logout':
            return <Link to="/logout"> Logout </Link>;
        case 'register':
            return <Link to="/register"> Register </Link>;
        case 'user':
            return <Link to="/user"> User settings </Link>;
        default:
            break;
    }
}