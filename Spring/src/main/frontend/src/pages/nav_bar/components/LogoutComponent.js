import React from "react";
import { Link, useHistory } from "react-router-dom";


export function LogoutComponent() {
    return (
        <div>
            <Link to="/logout"> Logout </Link>
        </div>
    );
}