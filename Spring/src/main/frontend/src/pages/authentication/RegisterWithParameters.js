import React from "react";
import { useParams } from "react-router-dom";
import { Register } from "./Register";

export function RegisterWithParameters() {
    const token = useParams();
    console.log(token);

    return (
        <div>
            <Register urlValue={token} />
        </div>
    );
}