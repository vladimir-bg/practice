import React from "react";
import axios from "axios";
import "./css/tabs.css";
import {Link, useHistory} from "react-router-dom";

export function Login() {
    let history = useHistory();
    let state = {
        fields: {
            email: '',
            password: ''
        },
        errors: {}
    }

    function loginHandler(e) {
        e.preventDefault();
        if(validate()){
            axios.post('http://localhost:8080/api/login', state.fields, {withCredentials: true})
                .then( response => {
                    history.push("/");
                    console.log('runed');
                }).catch(error => {
                let errors = {};
                    if(error.response !== undefined && error.response.data === 'Check email for account activation') {
                        errors["email"] = "Account is not activated";
                } else if(error.response !== undefined && error.response.data === 'Wrong username/password') {
                        errors["email"] = "Wrong username or password";
                    }
                this.setState({errors: errors});
            })
        }
    }

    function validate() {
        const{email, password} = state.fields;
        let errors = {};
        let formIsValid = true;

        if(!/^[a-zA-Z0-9]+@(?:[a-zA-Z0-9]+\.)+[A-Za-z]+$/.test(email)) {
            errors["email"] = "Email is not valid";
            formIsValid = false;
        } else if(email === "") {
            errors["email"] = "Email can not be empty";
            formIsValid = false;
        }

        if(password === "") {
            errors["password"] = "Password can not be empty";
            formIsValid = false;
        } else if(password.length < 8 || password.length > 30) {
            errors["password"] = "Password must be between 8 and 30 symbols";
            formIsValid = false;
        }

        state.errors = errors;
        return formIsValid;
    }

    function changeHandler(e) {
        if(e.target.name === 'email') {
            state.fields.email = e.target.value;
        } else {
            state.fields.password = e.target.value;
        }
    }

    document.getElementById('root')
        .style.backgroundImage = "linear-gradient(to bottom right, #78cba6, #77a0d4)";

    return (
        <div className="base-container">
            <h3 className={"header"}>Login</h3>
            <form onSubmit={loginHandler}>
                <div className="form">
                    <div className="form-group">
                        <label htmlFor="email">Email</label>
                        <input className="input"
                               type="text"
                               name="email"
                               placeholder="Email"
                               onChange={changeHandler}/>
                        <span className="errors">{state.errors["email"]}</span>
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input className="input"
                               type="password"
                               name="password"
                               placeholder="Password"
                               onChange={changeHandler}/>
                        <span className="errors">{state.errors["password"]}</span>
                    </div>
                </div>
                <div className="submit-button">
                    <button type="submit" className="btn">login</button>
                </div>
            </form>
            <div className="inline">
                <div className="redirects-right">
                    <p className="forgot-password">Forgot <Link to="/forgotPassword">password?</Link></p>
                </div>
                <div className="redirects-left">
                    <p className="forgot-password">Dont have account <Link to="/register">register?</Link></p>
                </div>
            </div>
        </div>
    );
}
