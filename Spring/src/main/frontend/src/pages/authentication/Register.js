import React from "react";
import { Link } from "react-router-dom"
import axios from "axios";
import "./css/tabs.css"

export class Register extends React.Component {
    constructor(properties) {
        super(properties);
        this.state = {
            fields: {
                email: '',
                firstName: '',
                lastName: '',
                password: '',
                confirmPassword: ''
            },
            errors: {}
        }
    }

    registerHandler = e => {
        e.preventDefault()
        if(this.validate()){
            axios
                .post('http://localhost:8080/api/register', this.state.fields)
                .then(response => {
                    //need to add dialog to say to check email
                }).catch(error => {
                if(error.response.data === 'This email is already registered') {
                    let errors = {};
                    errors["email"] = "Email is already registered";
                    this.setState({errors: errors});
                }
            })
        }
    }

    registerConfirmationHandler = e => {
        axios
            .get('http://localhost:8080/api/register/verification/' + e)
            .then(response => {
                //need to add dialog to say to check email
            }).catch(error => {
            if(error.response !== undefined && error.response.data === 'This email is already activated') {
                let errors = {};
                errors["email"] = "Email is already registered";
                this.setState({errors: errors});
            }
        })
    }

    validate() {
       const{email, firstName, lastName, password, confirmPassword} = this.state.fields;
       let errors = {};
       let formIsValid = true;

       if(!/^[a-zA-Z0-9]+@(?:[a-zA-Z0-9]+\.)+[A-Za-z]+$/.test(email)) {
           errors["email"] = "Email is not valid";
           formIsValid = false;
       } else if(email === "") {
           errors["email"] = "Email can not be empty";
           formIsValid = false;
       }

       if(firstName === "") {
           errors["firstName"] = "First name can not be empty";
           formIsValid = false;
       } else if(firstName.length < 3 || firstName.length > 25) {
           errors["firstName"] = "First name must be between 3 and 25 symbols";
           formIsValid = false;
       }

       if(lastName === "") {
           errors["lastName"] = "Last name can not be empty";
           formIsValid = false;
       } else if(lastName.length < 3 || lastName.length > 25) {
           errors["lastName"] = "Last name must be between 3 and 25 symbols";
           formIsValid = false;
       }

       if(password === "") {
           errors["password"] = "Password can not be empty";
           formIsValid = false;
       } else if(password.length < 8 || password.length > 30) {
           errors["password"] = "Password must be between 8 and 30 symbols";
           formIsValid = false;
       }

       if(confirmPassword !== password) {
           errors["confirmPassword"] = "Passwords must be equal";
           formIsValid = false;
       }

       this.setState({errors: errors});
       return formIsValid;
    }

    changeHandler = e => {
        let fields = this.state.fields;
        fields[e.target.name] = e.target.value;
        this.setState({fields});
    }

    render() {
        document.getElementById('root')
            .style.backgroundImage = "linear-gradient(to bottom right, #78cba6, #77a0d4)";
        const{email, firstName, lastName, password, confirmPassword} = this.state.fields;
        const token = this.props.urlValue;
        if(token) {
            this.registerConfirmationHandler(token);
            return <div className="base-container">
                <h3 className={"header"}>Your account is activated</h3>
            </div>
        } else return <div className="base-container">
            <h3 className={"header"}>Register</h3>
            <form onSubmit={this.registerHandler}>
                <div className="form">
                    <div className="form-group">
                        <label htmlFor="email">Email</label>
                        <input className="input"
                               type="email"
                               name="email"
                               placeholder="Email"
                               value={email}
                               onChange={this.changeHandler}/>
                        <span className="errors">{this.state.errors["email"]}</span>
                    </div>
                    <div className="form-group">
                        <label htmlFor="firstName">FirstName</label>
                        <input className="input"
                               type="text"
                               name="firstName"
                               placeholder="FirstName"
                               value={firstName}
                               onChange={this.changeHandler}/>
                        <span className="errors">{this.state.errors["firstName"]}</span>
                    </div>
                    <div className="form-group">
                        <label htmlFor="lastName">LastName</label>
                        <input className="input"
                               type="text"
                               name="lastName"
                               placeholder="LastName"
                               value={lastName}
                               onChange={this.changeHandler}/>
                        <span className="errors">{this.state.errors["lastName"]}</span>
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password</label>
                        <input className="input"
                               type="password"
                               name="password"
                               placeholder="Password"
                               value={password}
                               onChange={this.changeHandler}/>
                        <span className="errors">{this.state.errors["password"]}</span>
                    </div>
                    <div className="form-group">
                        <label htmlFor="confirm-password">Confirm Password</label>
                        <input className="input"
                               type="password"
                               name="confirmPassword"
                               placeholder="Confirm Password"
                               value={confirmPassword}
                               onChange={this.changeHandler}/>
                        <span className="errors">{this.state.errors["confirmPassword"]}</span>
                    </div>
                </div>
                <div className="submit-button">
                    <button type="submit" className="btn">register</button>
                </div>
            </form>
            <div className="redirects-right">
                <p className="forgot-password">Forgot <Link to="/forgotPassword">password?</Link></p>
            </div>
        </div>
    }
}