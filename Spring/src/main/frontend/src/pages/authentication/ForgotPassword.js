import React from "react";
import { Link } from "react-router-dom"
import axios from "axios";
import "./css/tabs.css"

export class ForgotPassword extends React.Component {
    token = this.props.urlValue
    constructor(properties) {
        super(properties);
        this.state = {
            fields: {},
            errors: {}
        };
    }

    forgotPasswordHandler = e => {
        e.preventDefault()
        if(this.validateEmail()){
            axios
                .get('http://localhost:8080/api/forgot-password', this.state.fields)
                .then(response => {
                    console.log(response.data)
                }).catch(error => {
                    if(error.response !== undefined && error.response.data === 'User not found') {
                        let errors = {};
                        errors["email"] = "User with this email is not found";
                        this.setState({errors: errors});
                    }
            })
        }
    }

    changePasswordHandler = e => {
        e.preventDefault()
        if(this.validatePassword()){
            axios
                .post('http://localhost:8080/api/forgot-password/' + this.token, this.state.fields)
                .then(response => {
                    console.log(response.data)
                }).catch(error => {
                if(error.response !== undefined && error.response.data === 'User not found') {
                    let errors = {};
                    errors["email"] = "User with this email is not found";
                    this.setState({errors: errors});
                }
            })
        }
    }

    validateEmail() {
        const email = this.state.fields.email;
        let errors = {};
        let formIsValid = true;

        if(!/^[a-zA-Z0-9]+@(?:[a-zA-Z0-9]+\.)+[A-Za-z]+$/.test(email)) {
            errors["email"] = "Email is not valid";
            formIsValid = false;
        } else if(email === "") {
            errors["email"] = "Email can not be empty";
            formIsValid = false;
        }

        this.setState({errors: errors});
        return formIsValid;
    }

    validatePassword() {
        const password = this.state.fields.password;
        const confirmPassword = this.state.fields.confirmPassword;
        let errors = {};
        let formIsValid = true;

        if(password === "") {
            errors["password"] = "Password can not be empty";
            formIsValid = false;
        } else if(password.length < 8 || password.length > 30) {
            errors["password"] = "Password must be between 8 and 30 symbols";
            formIsValid = false;
        }

        if(confirmPassword !== password) {
            errors["confirmPassword"] = "Passwords must be equal";
            formIsValid = false;
        }
        this.setState({errors: errors});
        return formIsValid;
    }

    changeHandler = e => {
        let fields = this.state.fields;
        fields[e.target.name] = e.target.value;
        this.setState({fields});
    }

    render() {
        document.getElementById('root')
            .style.backgroundImage = "linear-gradient(to bottom right, #78cba6, #77a0d4)";
        if(this.token){
            return <div className="base-container">
                <form onSubmit={this.changePasswordHandler}>
                    <div className="form">
                     <div className="form-group">
                            <label htmlFor="password">Password</label>
                            <input className="input"
                                type="password"
                                name="password"
                                placeholder="Password"
                                onChange={this.changeHandler}/>
                            <span className="errors">{this.state.errors["password"]}</span>
                        </div>
                        <div className="form-group">
                            <label htmlFor="confirm-password">Confirm Password</label>
                            <input className="input"
                                type="password"
                                name="confirmPassword"
                                placeholder="Confirm Password"
                                onChange={this.changeHandler}/>
                            <span className="errors">{this.state.errors["confirmPassword"]}</span>
                        </div>
                    </div>
                    <div className="submit-button">
                        <button type="submit" className="btn">register</button>
                    </div>
                </form>
            </div>
        } else {
            return <div className="base-container">
                <h3 className={"header"}>Change Password</h3>
                <form onSubmit={this.forgotPasswordHandler}>
                    <div className="form">
                        <div className="form-group">
                            <label htmlFor="email">Email</label>
                            <input className="input"
                                   type="text"
                                   name="email"
                                   placeholder="Email"
                                   onChange={this.changeHandler}/>
                            <span className="errors">{this.state.errors["email"]}</span>
                        </div>
                    </div>
                    <div className="submit-button">
                        <button type="submit" className="btn">send email</button>
                    </div>
                </form>
                <div className="redirects-right">
                    <p className="forgot-password"><Link to="/login">login</Link></p>
                </div>
            </div>
        }
    }
}