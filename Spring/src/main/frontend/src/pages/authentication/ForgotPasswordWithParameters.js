import React from "react";
import { useParams } from "react-router-dom";
import { ForgotPassword } from "./ForgotPassword";

export function ForgotPasswordWithParameters() {
    const token = useParams();
    console.log(token);

    return (
        <div>
            <ForgotPassword urlValue={token} />
        </div>
    );
}