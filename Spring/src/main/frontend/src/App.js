import './App.css';
import { Switch, Route } from "react-router-dom"
import { Router } from 'react-router';

import { Login } from "./pages/authentication/Login";
import { Register } from "./pages/authentication/Register";
import { ForgotPassword } from "./pages/authentication/ForgotPassword";
import { NavBar } from "./pages/nav_bar/NavBar";
import { Home } from "./pages/home/Home";
import { RegisterWithParameters } from "./pages/authentication/RegisterWithParameters";
import { ForgotPasswordWithParameters } from "./pages/authentication/ForgotPasswordWithParameters";
import { Logout } from "./pages/authentication/Logout";

import { createBrowserHistory } from "history";
export const history = createBrowserHistory({ forceRefresh: true });

function App() {
  return (
      <Router history={history}>
          <div className="App">
              <NavBar/>
          </div>

          <Switch>
              <Route exact path="/">
                  <Home/>
              </Route>

              <Route path="/login">
                  <Login/>
              </Route>

              <Route path="/register/:token">
                  <RegisterWithParameters/>
              </Route>

              <Route path="/register">
                  <Register/>
              </Route>

              <Route path="/home">
                  <ForgotPassword/>
              </Route>

              <Route path="/forgotPassword/:token">
                  <ForgotPasswordWithParameters/>
              </Route>

              <Route path="/forgotPassword">
                  <ForgotPassword/>
              </Route>

              <Route path="/logout">
                  <Logout/>
              </Route>
          </Switch>
      </Router>
  );
}

export default App;
