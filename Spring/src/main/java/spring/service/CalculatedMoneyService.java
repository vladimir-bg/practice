package spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.models.CalculatedMoney;
import spring.repository.CalculatedMoneyRepository;

import java.util.List;

@Service
public class CalculatedMoneyService {
    private final CalculatedMoneyRepository calculatedMoneyRepository;

    @Autowired
    public CalculatedMoneyService(CalculatedMoneyRepository calculatedMoneyRepository) {
        this.calculatedMoneyRepository = calculatedMoneyRepository;
    }

    public List<CalculatedMoney> getAll() {
        return calculatedMoneyRepository.findAll();
    }

    public CalculatedMoney getById(int id) {
        return calculatedMoneyRepository.findById(id).get();
    }

    public List<CalculatedMoney> getByUser(int userId) {
        return calculatedMoneyRepository.findByUser(userId);
    }

    public List<CalculatedMoney> getByUserAndType(int userId, String type) {
        return calculatedMoneyRepository.findByUserAndCalculatedMoneyType(userId, type);
    }

    public void create(CalculatedMoney calculatedMoney) {
        calculatedMoneyRepository.save(calculatedMoney);
    }
}
