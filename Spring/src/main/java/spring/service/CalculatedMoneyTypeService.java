package spring.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.models.CalculatedMoneyType;
import spring.repository.CalculatedMoneyTypeRepository;

import java.util.List;

@Service
public class CalculatedMoneyTypeService {
    private final CalculatedMoneyTypeRepository calculatedMoneyTypeRepository;

    @Autowired
    public CalculatedMoneyTypeService(CalculatedMoneyTypeRepository calculatedMoneyTypeRepository) {
        this.calculatedMoneyTypeRepository = calculatedMoneyTypeRepository;
    }

    public List<CalculatedMoneyType> getAll() {
        return calculatedMoneyTypeRepository.getAll();
    }
}
