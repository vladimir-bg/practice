package spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.models.SpendMoney;
import spring.repository.SpendMoneyRepository;

import java.util.Date;
import java.util.List;

@Service
public class SpendMoneyService {
    private final SpendMoneyRepository spendMoneyRepository;

    @Autowired
    public SpendMoneyService(SpendMoneyRepository spendMoneyRepository) {
        this.spendMoneyRepository = spendMoneyRepository;
    }

    public SpendMoney getById(int id) {
        return spendMoneyRepository.getById(id);
    }

    public List<SpendMoney> getByUserId(int userId) {
        return spendMoneyRepository.getByUserId(userId);
    }

    public List<SpendMoney> getByEmailAndDate(String email, Date date) {
        return spendMoneyRepository.getByEmailAndDate(email, date);
    }

    public void create(SpendMoney spendMoney) {
        spendMoneyRepository.create(spendMoney);
    }

    public void delete(int id) {
        spendMoneyRepository.delete(id);
    }
}
