package spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import spring.models.User;
import spring.repository.UserRepository;

import java.util.List;
import java.util.Random;

@Service
public class UserService {
   private final UserRepository userRepository;

   @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> getAll() {
        return userRepository.findAll();
    }

    public User getById(Long id) {
        return userRepository.findById(id).get();
    }

    public User getByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public User getByEmailAndPassword(String email, String password) {
        return userRepository.findByEmailAndPassword(email, password);
    }

    public User getByPasswordToken(String token) {
        return userRepository.findByForgotPasswordToken(token);
    }

    public User getByVerificationToken(String token) {
        return userRepository.findByVerification(token);
    }

    public void create(User user) {
        userRepository.save(user);
    }

    public void update(User user) {
        userRepository.save(user);
    }

    public void delete(Long id) {
        userRepository.delete(
                userRepository.findById(id).get());
    }

    public String generatePasswordToken(User user) {
       String token = generateToken();
       user.setForgotPasswordToken(token);
       userRepository.save(user);
       return token;
    }

    public String generateUserActivationToken(User user) {
        String token = generateToken();
        user.setVerification(token);
        userRepository.save(user);
        return token;
    }

    private String generateToken() {
        Random random = new Random();
        StringBuilder token = new StringBuilder();
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (int i = 0; i < 64; i++)
            token.append(
                    alphabet.charAt(
                            random.nextInt(
                                    alphabet.length())));
        return token.toString();
    }
}
