package spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.WebUtils;
import spring.models.User;
import spring.accounting.jwt.JwtProvider;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

@Service
public class CookieService {
    private final JwtProvider jwtProvider;
    private final UserService userService;

    @Autowired
    public CookieService(JwtProvider jwtProvider, UserService userService) {
        this.jwtProvider = jwtProvider;
        this.userService = userService;
    }

    public String getEmailFromCookie(HttpServletRequest request) {
        return getEmailFromRequestCookie(request);
    }

    public String getFirstNameFromCookie(HttpServletRequest request) {
        User user = userService.getByEmail(getEmailFromRequestCookie(request));
        return user.getFirstName();
    }

    public String getLastNameFromCookie(HttpServletRequest request) {
        User user = userService.getByEmail(getEmailFromRequestCookie(request));
        return user.getLastName();
    }

    public User getUserFromCookie(HttpServletRequest request) {
        return userService.getByEmail(getEmailFromRequestCookie(request));
    }

    public Cookie getCookieFromUrl(HttpServletRequest request) {
        return getCookieFromRequest(request);
    }

    private Cookie getCookieFromRequest(HttpServletRequest request) {
        return WebUtils.getCookie(request, "Jwt");
    }

    private String jwtFromRequestCookie(HttpServletRequest request) {
        Cookie cookie = getCookieFromRequest(request);
        if (cookie != null) return cookie.getValue();
        else return null;
    }

    private String getEmailFromJwt(String jwt) {
        return jwtProvider.getEmailFromToken(jwt);
    }

    private String getEmailFromRequestCookie(HttpServletRequest request) {
        return getEmailFromJwt(jwtFromRequestCookie(request));
    }
}
