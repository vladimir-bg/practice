package spring.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import spring.emailService.GmailSenderService;
import spring.exceptions.EntityNotFoundException;
import spring.exceptions.UserPasswordException;
import spring.models.User;
import spring.accounting.jwt.JwtProvider;
import spring.service.CookieService;
import spring.service.UserService;
import spring.web.dtos.user.*;
import spring.web.mappers.user.ChangePasswordModelMapper;
import spring.web.mappers.user.LoginModelMapper;
import spring.web.mappers.user.RegisterModelMapper;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@CrossOrigin(origins = "http://localhost:3000", allowCredentials = "true")
@RestController
@RequestMapping("/api")
public class AuthorizationController {
    private final UserService userService;
    private final RegisterModelMapper registerModelMapper;
    private final LoginModelMapper loginModelMapper;
    private final ChangePasswordModelMapper changePassword;
    private final JwtProvider jwtProvider;
    private final GmailSenderService gmailSenderService;
    private final CookieService cookieService;

    @Autowired
    public AuthorizationController(UserService userService,
                                   RegisterModelMapper registerModelMapper,
                                   LoginModelMapper loginModelMapper,
                                   ChangePasswordModelMapper changePassword,
                                   JwtProvider jwtProvider,
                                   GmailSenderService gmailSenderService,
                                   CookieService cookieService) {
        this.userService = userService;
        this.registerModelMapper = registerModelMapper;
        this.loginModelMapper = loginModelMapper;
        this.changePassword = changePassword;
        this.jwtProvider = jwtProvider;
        this.gmailSenderService = gmailSenderService;
        this.cookieService = cookieService;
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@Valid @RequestBody RegisterDto registerDto) {
        User user = registerModelMapper.fromDto(registerDto);
        if(userService.getByEmail(user.getEmail()) != null)
            return new ResponseEntity<>("User with this email is already registered", HttpStatus.CONFLICT);
        userService.create(user);
        String url = userService.generateUserActivationToken(user);
        System.out.println("Account verification: " + "localhost:3000/api/register/verification/" + url);
        //gmailSenderService.sendEmail(user.getEmail(), "Account verification", "localhost:3000/api/register/verification/" + url);
        //is commented for testing
        return new ResponseEntity<>("Email is sent for verification", HttpStatus.OK);
    }

    @GetMapping("/register/verification/{token}")
    public void registerValidation(@Valid @PathVariable String token) {
        User user = userService.getByVerificationToken(token);
        if(!user.isActivated()) {
            user.setActivated(true);
            userService.update(user);
        }
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(HttpServletResponse response,
                                   @Valid @RequestBody LoginDto loginDto) {
        User user;
        try {
            user = loginModelMapper.fromDto(loginDto);
        } catch (UserPasswordException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }

        if(!user.isActivated())
            return new ResponseEntity<>("Check email for account activation", HttpStatus.CONFLICT);
        Cookie cookie = new Cookie("Jwt", jwtProvider.generateToken(user.getEmail()));
        cookie.setPath("/");
        cookie.setHttpOnly(true);
        response.addCookie(cookie);
        return new ResponseEntity<>("Login successful", HttpStatus.OK);
    }

    @GetMapping("/logout")
    public ResponseEntity<?> logout(HttpServletRequest request,
                                    HttpServletResponse response) {
        Cookie cookie = cookieService.getCookieFromUrl(request);
        cookie.setPath("/");
        cookie.setHttpOnly(true);
        cookie.setValue(null);
        cookie.setMaxAge(0);
        response.addCookie(cookie);
        return new ResponseEntity<>("Logout successful", HttpStatus.OK);
    }

    @GetMapping("/forgot-password")
    public ResponseEntity<?> forgotPassword(@Valid @RequestBody ForgotPasswordDto dto) {
        User user;
        try {
            user = userService.getByEmail(dto.getEmail());
        } catch (EntityNotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
        String url = userService.generatePasswordToken(user);
        gmailSenderService.sendEmail(user.getEmail(), "Account verification",
                "localhost:3000/api/forgot-password/" + url);
        return new ResponseEntity<>("Email is send", HttpStatus.OK);
    }

    @PostMapping("/forgot-password/{token}")
    public User forgotPassword(@Valid @PathVariable String token,
                               @Valid @RequestBody NewPasswordDto dto) {
        User user = userService.getByPasswordToken(token);
        return changePassword.fromDto(dto, user);
    }

    @GetMapping("/token")
    public ResponseEntity<?> getToken(HttpServletRequest request) {
        User userFromCookie = cookieService.getUserFromCookie(request);
        if(userFromCookie != null) return new ResponseEntity<>("notGuest", HttpStatus.OK);
        else return new ResponseEntity<>("guest", HttpStatus.CONFLICT);
    }
}
