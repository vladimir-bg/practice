package spring.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring.models.User;
import spring.service.CookieService;
import spring.service.UserService;
import spring.web.dtos.user.ChangePasswordDto;
import spring.web.mappers.user.ChangePasswordModelMapper;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/api/user")
public class UserAccountController {
    private final UserService userService;
    private final ChangePasswordModelMapper changePassword;
    private final CookieService cookieService;

    @Autowired
    public UserAccountController(UserService userService,
                                 ChangePasswordModelMapper changePassword,
                                 CookieService cookieService) {
        this.userService = userService;
        this.changePassword = changePassword;
        this.cookieService = cookieService;
    }

    @PostMapping("/change-password")
    public User changeUserPassword(HttpServletRequest request,
                                   @Valid @RequestBody ChangePasswordDto dto) {
        User userFromCookie = cookieService.getUserFromCookie(request);
        User user = changePassword.fromDto(dto, userFromCookie);
        userService.update(user);
        return user;
    }
}