package spring.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import spring.models.SpendMoney;
import spring.service.CookieService;
import spring.service.SpendMoneyService;
import spring.service.UserService;
import spring.web.dtos.DailyMoneyDto;
import spring.web.dtos.GetByDateDto;
import spring.web.dtos.SpendMoneyDto;
import spring.web.mappers.GetByDateModelMapper;
import spring.web.mappers.SpendMoneyModelMapper;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/api/user/money")
public class UserMoneyController {
    private final UserService userService;
    private final SpendMoneyService spendMoneyService;
    private final SpendMoneyModelMapper spendMoneyModelMapper;
    private final GetByDateModelMapper getByDateModelMapper;
    private final CookieService cookieService;

    @Autowired
    public UserMoneyController(UserService userService,
                               SpendMoneyService spendMoneyService,
                               SpendMoneyModelMapper spendMoneyModelMapper,
                               GetByDateModelMapper getByDateModelMapper,
                               CookieService cookieService) {
        this.userService = userService;
        this.spendMoneyService = spendMoneyService;
        this.spendMoneyModelMapper = spendMoneyModelMapper;
        this.getByDateModelMapper = getByDateModelMapper;
        this.cookieService = cookieService;
    }

    @PostMapping("/spent-money")
    public SpendMoney addSpentMoney(HttpServletRequest request,
                                    @Valid @RequestBody SpendMoneyDto spendMoneyDto) {
        String email = cookieService.getEmailFromCookie(request);
        spendMoneyDto.setEmail(email);
        SpendMoney money = spendMoneyModelMapper.fromDto(spendMoneyDto);
        spendMoneyService.create(money);
        return money;
    }

    @GetMapping("/spent-daily-money")
    public List<DailyMoneyDto> getSpentMoney(HttpServletRequest request, @Valid @RequestBody GetByDateDto getByDateDto) {
        String email = cookieService.getEmailFromCookie(request);
        SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy");
        Date date = null;
        try {
            date = formatter.parse(getByDateDto.getDate());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<SpendMoney> monies = spendMoneyService.getByEmailAndDate(email, date);
        return getByDateModelMapper.fromDto(monies);
    }
}
