package spring.web.dtos;

import javax.validation.constraints.*;

public class SpendMoneyDto {
    @NotNull(message = "Product must not be null")
    @Size(min = 2, max = 20, message = "Product length must be between 2 and 20 symbols")
    private String product;

    @Positive(message = "Money must be positive")
    private double money;

    private String email;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
