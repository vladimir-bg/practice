package spring.web.dtos;

import javax.validation.constraints.NotNull;

public class GetByDateDto {
    @NotNull(message = "Date must be not null")
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
