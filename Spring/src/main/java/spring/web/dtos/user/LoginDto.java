package spring.web.dtos.user;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class LoginDto {
    @NotNull(message = "Username must not be empty")
    @Size(min = 20, max = 100, message = "Username must be between 3 and 20")
    private String email;

    @NotNull(message = "Password must not be empty")
    @Size(min = 8, max = 20, message = "Password must be between 8 and 20")
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
