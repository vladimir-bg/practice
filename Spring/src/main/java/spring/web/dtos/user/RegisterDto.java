package spring.web.dtos.user;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class RegisterDto {
    @NotNull(message = "Username must not be empty")
    @Size(min = 20, max = 100, message = "Username must be between 20 and 100")
    private String email;

    @NotNull(message = "First name must not be empty")
    @Size(min = 2, max = 25, message = "First name must be between 2 and 25")
    private String firstName;

    @NotNull(message = "Last name must not be empty")
    @Size(min = 2, max = 25, message = "Last name must be between 2 and 25")
    private String lastName;

    @NotNull(message = "Password must not be empty")
    @Size(min = 8, max = 20, message = "Password must be between 8 and 20")
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
