package spring.web.mappers.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import spring.exceptions.UserPasswordException;
import spring.models.User;
import spring.service.UserService;
import spring.web.dtos.user.LoginDto;

@Component
public class LoginModelMapper {
    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public LoginModelMapper(UserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    public User fromDto(LoginDto loginDto) {
        User user = userService.getByEmail(loginDto.getEmail());
        return dtoToObject(loginDto, user);
    }

    private User dtoToObject(LoginDto loginDto, User user) {
        if(passwordEncoder.matches(loginDto.getPassword(), user.getPassword()))
            return userService.getByEmailAndPassword(loginDto.getEmail(), user.getPassword());
        throw new UserPasswordException("Wrong username/password");
    }
}
