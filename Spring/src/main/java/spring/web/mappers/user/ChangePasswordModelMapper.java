package spring.web.mappers.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import spring.exceptions.UserPasswordException;
import spring.models.User;
import spring.web.dtos.user.ChangePasswordDto;
import spring.web.dtos.user.NewPasswordDto;

@Component
public class ChangePasswordModelMapper {
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public ChangePasswordModelMapper(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public User fromDto(ChangePasswordDto dto, User user) {
        if(dto.getOldPassword().equals(dto.getNewPassword())) throw new UserPasswordException("Password must be different than old");
        if(passwordEncoder.matches(dto.getOldPassword(), user.getPassword())) {
            user.setPassword(passwordEncoder.encode(dto.getNewPassword()));
            return user;
        } else throw new UserPasswordException("Password do not match");
    }

    public User fromDto(NewPasswordDto dto, User user) {
        changePassword(user, dto);
        return user;
    }

    private void changePassword(User user, NewPasswordDto dto) {
        user.setPassword(passwordEncoder.encode(dto.getPassword()));
        user.setForgotPasswordToken(null);
    }
}
