package spring.web.mappers.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import spring.models.User;
import spring.web.dtos.user.RegisterDto;

@Component
public class RegisterModelMapper {
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public RegisterModelMapper(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public User fromDto(RegisterDto registerDto) {
        User user = new User();
        dtoToObject(user, registerDto);
        return user;
    }

    private void dtoToObject(User user, RegisterDto registerDto) {
        user.setEmail(registerDto.getEmail());
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.setPassword(passwordEncoder.encode(registerDto.getPassword()));
    }
}
