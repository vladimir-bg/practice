package spring.web.mappers;

import org.springframework.stereotype.Component;
import spring.models.SpendMoney;
import spring.models.User;
import spring.service.UserService;
import spring.web.dtos.SpendMoneyDto;

import java.util.Date;

@Component
public class SpendMoneyModelMapper {
    private final UserService userService;

    public SpendMoneyModelMapper(UserService userService) {
        this.userService = userService;
    }

    public SpendMoney fromDto(SpendMoneyDto spendMoneyDto) {
        SpendMoney money = new SpendMoney();
        dtoToObject(money, spendMoneyDto);
        return money;
    }

    private void dtoToObject(SpendMoney money, SpendMoneyDto spendMoneyDto) {
        User user = userService.getByEmail(spendMoneyDto.getEmail());
        money.setProduct(spendMoneyDto.getProduct());
        money.setAmount(spendMoneyDto.getMoney());
        money.setCreationDate(new Date());
        money.setUser(user);
    }
}
