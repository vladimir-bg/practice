package spring.web.mappers;

import org.springframework.stereotype.Component;
import spring.models.SpendMoney;
import spring.web.dtos.DailyMoneyDto;

import java.util.ArrayList;
import java.util.List;

@Component
public class GetByDateModelMapper {
    public List<DailyMoneyDto> fromDto(List<SpendMoney> monies) {
        List<DailyMoneyDto> dto = new ArrayList<>();
        dtoToObject(monies, dto);
        return dto;
    }

    private void dtoToObject(List<SpendMoney> monies, List<DailyMoneyDto> dto) {
        for (int i = 0; i <monies.size() - 1; i++) {
            dto.get(i).setProduct(monies.get(i).getProduct());
            dto.get(i).setMoney(monies.get(i).getAmount());
        }
    }
}
