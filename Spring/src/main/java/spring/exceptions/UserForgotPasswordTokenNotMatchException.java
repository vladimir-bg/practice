package spring.exceptions;

public class UserForgotPasswordTokenNotMatchException extends RuntimeException {
    public UserForgotPasswordTokenNotMatchException(String message) {
        super(message);
    }
}
