package spring.exceptions;

public class UserExistingException extends RuntimeException {
    public UserExistingException(String message) {
        super(message);
    }
}
