package spring.exceptions;

public class UserSameAsOldPasswordException extends RuntimeException{
    public UserSameAsOldPasswordException(String message) {
        super(message);
    }
}
