package spring.accounting.users.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spring.exceptions.EntityNotFoundException;
import spring.models.User;
import spring.accounting.users.UserDetailsImpl;
import spring.service.UserService;

@Component
public class UserBuilderUtil {
    private final UserService userService;

    @Autowired
    public UserBuilderUtil(UserService userService) {
        this.userService = userService;
    }

    public UserDetailsImpl buildFromUsername(String username) {
        User user = userService.getByEmail(username);
        UserDetailsImpl userDetails;
        if(user != null) {
            userDetails = new UserDetailsImpl();
            userDetails.setUser(user);
        } else throw new EntityNotFoundException("User doesn't exist with this username: " + username);
        return userDetails;
    }
}
