package spring.accounting.jwt;

import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class JwtProvider {
    @Value("$(jwt.secret)")
    private String kay;

    public String generateToken(String email) {
        return Jwts
                    .builder()
                    .setSubject(email)
                    .signWith(SignatureAlgorithm.HS512, kay)
                    .compact();
    }

    public boolean validateToken(String token) {
        try {
            Jwts
                    .parser()
                    .setSigningKey(kay)
                    .parseClaimsJws(token);
            return true;
        } catch (ExpiredJwtException
                | UnsupportedJwtException
                | MalformedJwtException
                | SignatureException
                | IllegalArgumentException a) {
            System.out.println(a);
        }
        return false;
    }

    public String getEmailFromToken(String token) {
        Claims claims = Jwts
                .parser()
                .setSigningKey(kay)
                .parseClaimsJws(token)
                .getBody();
        return claims.getSubject();
    }
}
