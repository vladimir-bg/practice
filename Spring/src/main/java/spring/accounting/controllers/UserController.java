package spring.accounting.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import spring.accounting.dtos.response.LoginResponse;
import spring.web.dtos.user.LoginDto;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/public")
public class UserController {

    public ResponseEntity<LoginResponse> login(HttpServletResponse response,
                                               @Valid @RequestBody LoginDto loginDto) {

    }

    public void register() {

    }

    public void forgotPassword() {

    }

    public void changePassword() {

    }

    public void getToken() {

    }

    public void refreshToken() {

    }
}
