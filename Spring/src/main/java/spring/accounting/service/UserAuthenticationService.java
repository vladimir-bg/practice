package spring.accounting.service;

import org.springframework.security.core.userdetails.User;
import spring.web.dtos.user.LoginDto;
import spring.web.dtos.user.RegisterDto;

import java.util.Optional;

public interface UserAuthenticationService {
    Optional<String> register(RegisterDto registerDto);
    Optional<String> login(LoginDto loginDto);
    Optional<String> logout(User user);
}
