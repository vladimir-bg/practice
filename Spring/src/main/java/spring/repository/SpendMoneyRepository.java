package spring.repository;

import org.springframework.stereotype.Repository;
import spring.models.SpendMoney;

import java.util.Date;
import java.util.List;

@Repository
public interface SpendMoneyRepository {
    SpendMoney getById(int id);
    List<SpendMoney> getByUserId(int userId);
    List<SpendMoney> getByEmailAndDate(String email, Date date);
    void create(SpendMoney spendMoney);
    void delete(int id);
}
