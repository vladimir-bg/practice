package spring.repository;

import org.springframework.stereotype.Repository;
import spring.models.CalculatedMoneyType;

import java.util.List;

@Repository
public interface CalculatedMoneyTypeRepository {
    List<CalculatedMoneyType> getAll();
}
