package spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import spring.models.CalculatedMoney;

import java.util.List;

@Repository
public interface CalculatedMoneyRepository extends JpaRepository<CalculatedMoney, Integer> {
    List<CalculatedMoney> findByUser(int userId);
    List<CalculatedMoney> findByUserAndCalculatedMoneyType(int userId, String type);
}
