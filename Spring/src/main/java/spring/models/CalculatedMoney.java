package spring.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "calculated_money")
public class CalculatedMoney {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @Column(name = "date_from")
    private Date dateFrom;

    @Column(name = "date_to")
    private Date dateTo;

    @Column(name = "amount")
    private double amount;

    @ManyToOne
    @JoinColumn(name = "type_id")
    private CalculatedMoneyType calculatedMoneyType;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public CalculatedMoneyType getCalculatedMoneyType() {
        return calculatedMoneyType;
    }

    public void setCalculatedMoneyType(CalculatedMoneyType calculatedMoneyType) {
        this.calculatedMoneyType = calculatedMoneyType;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
