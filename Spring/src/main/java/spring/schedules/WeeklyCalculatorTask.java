package spring.schedules;

import org.springframework.scheduling.annotation.Scheduled;
import spring.models.CalculatedMoney;
import spring.models.CalculatedMoneyType;
import spring.models.User;
import spring.service.CalculatedMoneyService;
import spring.service.CalculatedMoneyTypeService;
import spring.service.UserService;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class WeeklyCalculatorTask {
    private final UserService userService;
    private final CalculatedMoneyService calculatedMoneyService;
    private final CalculatedMoneyTypeService calculatedMoneyTypeService;

    public WeeklyCalculatorTask(UserService userService, CalculatedMoneyService calculatedMoneyService, CalculatedMoneyTypeService calculatedMoneyTypeService) {
        this.userService = userService;
        this.calculatedMoneyService = calculatedMoneyService;
        this.calculatedMoneyTypeService = calculatedMoneyTypeService;
    }

    @Scheduled(cron = "0 0 0 * * *", zone = "Europe/Sofia")
    public void weeklyJob() {
        List<User> users = userService.getAll();
        for (User user : users) {
            CalculatedMoney calculatedMoney = new CalculatedMoney();
            calculatedMoney.setUser(user);
            calculatedMoney.setDateFrom(getDateMinusOneWeek());
            calculatedMoney.setDateTo(new Date());
            calculatedMoney.setAmount(
                    sumOfMoney(
                            createListByUser(user)));
            List<CalculatedMoneyType> list = calculatedMoneyTypeService.getAll();
            calculatedMoney.setCalculatedMoneyType(list
                    .stream()
                    .filter(e -> "weekly".equals(e.getType()))
                    .findAny()
                    .orElse(null));
            calculatedMoneyService.create(calculatedMoney);
        }
    }

    private List<CalculatedMoney> createListByUser(User user) {
        return calculatedMoneyService
                .getByUserAndType(user.getId(), "daily")
                .stream()
                .filter(e -> e.getDateTo().before(new Date())
                        && e.getDateFrom().after(getDateMinusOneWeek()))
                .collect(Collectors.toList());
    }

    private Date getDateMinusOneWeek() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, -7);
        return calendar.getTime();
    }

    private double sumOfMoney(List<CalculatedMoney> calculatedMonies) {
        double sum = 0;
        for (CalculatedMoney calculatedMoney : calculatedMonies) {
            sum += calculatedMoney.getAmount();
        }
        return sum;
    }
}
