package spring.schedules;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import spring.models.CalculatedMoney;
import spring.models.CalculatedMoneyType;
import spring.models.SpendMoney;
import spring.models.User;
import spring.service.CalculatedMoneyService;
import spring.service.CalculatedMoneyTypeService;
import spring.service.SpendMoneyService;
import spring.service.UserService;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class DailyCalculatorTask {
    private final UserService userService;
    private final SpendMoneyService spendMoneyService;
    private final CalculatedMoneyService calculatedMoneyService;
    private final CalculatedMoneyTypeService calculatedMoneyTypeService;

    public DailyCalculatorTask(UserService userService, SpendMoneyService spendMoneyService, CalculatedMoneyService calculatedMoneyService, CalculatedMoneyTypeService calculatedMoneyTypeService) {
        this.userService = userService;
        this.spendMoneyService = spendMoneyService;
        this.calculatedMoneyService = calculatedMoneyService;
        this.calculatedMoneyTypeService = calculatedMoneyTypeService;
    }

    @Scheduled(cron = "0 0 0 * * *", zone = "Europe/Sofia")
    public void dailyJob() {
        List<User> userList = userService.getAll();
        for (User user : userList) {
            List<SpendMoney> money = spendMoneyService.getByEmailAndDate(user.getEmail(), getDateMinusOneDay());
            CalculatedMoney calculatedMoney = new CalculatedMoney();
            calculatedMoney.setUser(user);
            calculatedMoney.setDateFrom(getDateMinusOneDay());
            calculatedMoney.setDateTo(new Date());
            calculatedMoney.setAmount(sumOfMoney(money));
            List<CalculatedMoneyType> list = calculatedMoneyTypeService.getAll();
            calculatedMoney.setCalculatedMoneyType(list
                    .stream()
                    .filter(e -> "daily".equals(e.getType()))
                    .findAny()
                    .orElse(null));
            calculatedMoneyService.create(calculatedMoney);
        }
    }

    private Date getDateMinusOneDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, -1);
        return calendar.getTime();
    }

    private double sumOfMoney(List<SpendMoney> money) {
        double sum = 0;
        for (SpendMoney spendMoney : money) {
            sum += spendMoney.getAmount();
        }
        return sum;
    }
}
