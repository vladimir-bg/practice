package spring.emailService;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.gmail.Gmail;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

public class GmailConnectionService {
    private static final String APPLICATION_NAME = "Money Tracker";
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static final File credentials =
            new File("D:\\repos\\practice\\Spring\\src\\main\\java\\spring\\emailService\\credentials.json");

    public Gmail getGmailService() throws IOException, GeneralSecurityException {
        InputStream credentialsInput = new FileInputStream(credentials);
        GoogleClientSecrets clientSecrets =
                GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(credentialsInput));
        Credential credential = new GoogleCredential
                .Builder()
                .setTransport(GoogleNetHttpTransport.newTrustedTransport())
                .setJsonFactory(JSON_FACTORY)
                .setClientSecrets(clientSecrets
                        .getDetails()
                        .getClientId(), clientSecrets
                      .getDetails()
                      .getClientSecret())
                .build()
                .setAccessToken(getAccessToken())
                .setRefreshToken("1%2F%2F044hSJ2AfBxbbCgYIARAAGAQSNwF-L9IrtiunnkLksB3Rvoo4FNx-8-PGPmH9iixJxv38MBQhGTJ4N9bNnsSRb3rggmUgnz9Mw7k");
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        return new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
                .setApplicationName(APPLICATION_NAME).build();
    }

    private String getAccessToken() {
        try {
            Map<String, String> parameters = new HashMap<>();
            parameters.put("grant_type", "refresh_token");
            parameters.put("client_id", "743112968170-kljuc3ls0ustdibloboduahjkfr47dn1.apps.googleusercontent.com");
            parameters.put("client_secret", "X99lEzzJkFzsjvOQrKWdkoa_");
            parameters.put("refresh_token", "1//044hSJ2AfBxbbCgYIARAAGAQSNwF-L9IrtiunnkLksB3Rvoo4FNx-8-PGPmH9iixJxv38MBQhGTJ4N9bNnsSRb3rggmUgnz9Mw7k");

            StringBuilder data = new StringBuilder();
            for (Map.Entry<String, String> parameter : parameters.entrySet()) {
                if(data.length() != 0) data.append('&');
                data.append(URLEncoder.encode(parameter.getKey(), StandardCharsets.UTF_8));
                data.append('=');
                data.append(URLEncoder.encode(parameter.getValue(), StandardCharsets.UTF_8));
            }
            byte[] dataBytes = data.toString().getBytes(StandardCharsets.UTF_8);

            URL url = new URL("https://accounts.google.com/o/oauth2/token");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setUseCaches(false);
            connection.setRequestMethod("POST");
            connection.getOutputStream().write(dataBytes);

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            StringBuilder buffer = new StringBuilder();
            for (String line = reader.readLine(); line != null ; line = reader.readLine())
                buffer.append(line);

            return new JSONObject(buffer.toString()).getString("access_token");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
