package spring.emailService;

import com.google.api.client.util.Base64;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Properties;

@Component
public class GmailSenderService {
    private final String user = "me";

    public void sendMessage(String userId, MimeMessage email) {
        try {
            new GmailConnectionService()
                    .getGmailService()
                    .users()
                    .messages()
                    .send(userId, createEmailMessage(email))
                    .execute();
        } catch (MessagingException | GeneralSecurityException | IOException e) {
            e.printStackTrace();
        }
    }

    public void sendEmail(String receiver, String subject, String bodyText)  {
        try{
            Gmail service;
            service = new GmailConnectionService().getGmailService();
            MimeMessage email;
            email = createEmail(receiver, subject, bodyText);
            Message message = null;
            message = createEmailMessage(email);
            service
                    .users()
                    .messages()
                    .send(user, message)
                    .execute();
        } catch (IOException | MessagingException | GeneralSecurityException e) {
            e.printStackTrace();
        }

    }

    private Message createEmailMessage(MimeMessage email) throws MessagingException, IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        email.writeTo(byteArrayOutputStream);
        return new Message()
                .setRaw(Base64.encodeBase64URLSafeString(byteArrayOutputStream.toByteArray()));
    }

    private MimeMessage createEmail(String to, String subject, String bodyText) throws MessagingException {
        Properties properties = new Properties();
        Session session = Session.getDefaultInstance(properties, null);
        MimeMessage email = new MimeMessage(session);
        email.setFrom(new InternetAddress(user));
        email.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(to));
        email.setSubject(subject);
        email.setText(bodyText);
        return email;
    }
//TODO finish email service start implementing registration and forgot password email verification
}
