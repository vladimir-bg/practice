CREATE DATABASE IF NOT EXISTS `money_tracker_db`;
USE `money_tracker_db`;

create or replace table users
(
    id                    int auto_increment
    primary key,
    email                 varchar(100) not null,
    first_name            varchar(25)  not null,
    last_name             varchar(25)  not null,
    password              varchar(512) not null,
    verification          varchar(256) null,
    forgot_password_token varchar(256) null,
    constraint users_email_uindex
    unique (email)
);

create or replace table calculated_money_type
(
    id   int auto_increment
        primary key,
    type varchar(20) not null
);

create or replace table calculated_money
(
    id        int auto_increment
        primary key,
    date_from date   not null,
    date_to   date   not null,
    amount    double not null,
    type_id   int    not null,
    user_id   int    null,
    constraint calculated_money_calculated_money_type_fk
        foreign key (type_id) references money_tracker_db.calculated_money_type (id),
    constraint calculated_money_users_fk
        foreign key (user_id) references money_tracker_db.users (id)
);

create or replace table spent_money
(
    id            int auto_increment
    primary key,
    user_id       int      not null,
    amount        double   not null,
    product       tinytext not null,
    creation_date date     not null,
    constraint spend_money_users_fk
    foreign key (user_id) references money_tracker_db.users (id)
);