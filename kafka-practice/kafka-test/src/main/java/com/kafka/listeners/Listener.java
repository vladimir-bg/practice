package com.kafka.listeners;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class Listener {
    @KafkaListener(
            topics = "test",
            groupId = "groupId")
    public void listener(String data) {
        System.out.println(data);
    }
}
