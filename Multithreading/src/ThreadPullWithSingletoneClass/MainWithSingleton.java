package ThreadPullWithSingletoneClass;

public class MainWithSingleton {
    public static void main(String[] args) {
        for (int i = 0; i < 6; i++) {
            ExecutorPullWithSingleton.getInstance().executeTask(() -> {
                for (int z = 0; z < 10; z++) {
                    System.out.println("Run " + z);
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        ExecutorPullWithSingleton.getInstance().terminateExecutor();
    }
}
