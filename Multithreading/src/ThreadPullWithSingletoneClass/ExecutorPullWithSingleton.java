package ThreadPullWithSingletoneClass;
import java.util.concurrent.*;

public class ExecutorPullWithSingleton {
    private static volatile ExecutorPullWithSingleton instance;
    private final int availableProcessor = Runtime.getRuntime().availableProcessors();
    private final ExecutorService executorService = Executors.newFixedThreadPool(availableProcessor);
    private int executeThreadNum;

    private ExecutorPullWithSingleton() {
    }

    public static ExecutorPullWithSingleton getInstance() {
        if(instance == null)
            instance = new ExecutorPullWithSingleton();
        return instance;
    }

    public void executeTask(Runnable runnable) {
        System.out.println("Asynchronous thread executed " + executeThreadNum++ + " times.");
        executorService.submit(runnable);
    }

    public void terminateExecutor() {
        executorService.shutdown();
    }
}
