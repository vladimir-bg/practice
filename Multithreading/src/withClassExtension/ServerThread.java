package withClassExtension;

public class ServerThread {
    public static void main(String[] args) {
        MainWithClass server = new MainWithClass();
        System.out.println("thread is started");
        server.start();
    }
}
