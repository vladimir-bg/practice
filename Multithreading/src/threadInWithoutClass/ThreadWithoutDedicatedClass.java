package threadInWithoutClass;

public class ThreadWithoutDedicatedClass {
    public static void main(String[] args) {
        int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("CPU cores: " + processors);

        Thread t1 = new Thread(() -> {
            for (int i = 0; i < 10; i++) {
                System.out.println("Hello" + i);
            }
        });
        System.out.println("Thread start");
        t1.start();
    }
}
