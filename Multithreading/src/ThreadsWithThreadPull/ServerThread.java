package ThreadsWithThreadPull;

public class ServerThread implements Runnable{
    public String name;
    public ServerThread(String name) {
       this.name = name;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(name + " " + i);
        }
    }
}
