package ThreadsWithThreadPull;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class MainWithPull {
    public static void main(String[] args) {
        int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("CPU cores: " + processors);
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(processors);

        for (int i = 0; i < 24; i++) {
            Thread task = new Thread(new ServerThread("Task " + i));
            System.out.println("Created : " + task.getName());
            executor.execute(task);
        }
        executor.shutdown();
    }
}
