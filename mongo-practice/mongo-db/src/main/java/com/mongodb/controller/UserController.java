package com.mongodb.controller;

import com.mongodb.entity.User;
import com.mongodb.reoisitory.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/user")
public class UserController {
    private final UserRepository userRepository;

    @PostMapping
    public List<User> insertUser(@RequestBody User user) {
        return userRepository.findAll();
        //return userRepository.insert(user);
    }
}
