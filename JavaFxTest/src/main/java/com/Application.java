package com;

import com.io.serial.thread.SerialThread;
import javafx.stage.Stage;
import java.io.IOException;

public class Application extends javafx.application.Application{
    @Override
    public void start(Stage stage) throws IOException {
        GuiStart.start(stage);
    }

    public static void main(String[] args) {
        SerialThread.getInstance().start();
        launch();
    }
}