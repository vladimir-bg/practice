package com.io.serial.entity;

public class MapEntity {
    private final int value;
    private final int x;
    private final int y;

    public MapEntity(int value, int x, int y) {
        this.value = value;
        this.x = x;
        this.y = y;
    }

    public int getValue() {
        return value;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
