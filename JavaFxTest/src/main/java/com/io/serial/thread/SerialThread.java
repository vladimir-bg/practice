package com.io.serial.thread;

import java.util.Random;

public class SerialThread extends Thread {
    private static SerialThread serialThread;
    private static int[] ints;
    private static Random random = new Random();
    private SerialThread() {}

    public static SerialThread getInstance() {
        if (ints == null) {
            ints = new int[400];
        }
        if (serialThread == null) {
            serialThread = new SerialThread();
            serialThread.setDaemon(true);
            return serialThread;
        }
        return serialThread;
    }

    public static int[] getInts() {
        return ints;
    }

    @Override
    public void run() {
        while (true) {
            for (int i = 0; i < ints.length; i++) {
                ints[i] = random.nextInt() % 10;
            }
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
