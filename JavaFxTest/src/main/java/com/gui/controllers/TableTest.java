package com.gui.controllers;

import com.gui.entity.CoordinatedText;
import com.io.serial.entity.MapEntity;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TableTest {

    public static final short ROWS = 20;
    public static final short COLS = 20;
    public static final short SIZE_OF_MAP = ROWS * COLS;
    public static final short WIDTH = 50;
    public static final short HEIGHT = 20;

    @FXML
    BorderPane borderPane;

    @FXML
    volatile GridPane grid;

    @FXML
    public void initialize() {
        Test test = new Test();
        test.setDaemon(true);
        grid = createMap();
        borderPane.getChildren().add(grid);
        test.setGridPane(grid);
        test.start();
    }

    private class Test extends Thread {
        private GridPane gridPane;
        private int val = 0;
        public Test() {
        }

        public void setGridPane(GridPane gridPane) {
            this.gridPane = gridPane;
        }

        @Override
        public void run() {
            while (true) {
                Platform.runLater(() -> {
                    gridPane.getChildren().clear();
                    gridPane.getChildren().setAll(createMapWithValue(++val));
                });
                System.out.println(val);
                try {
                    Thread.sleep(100L);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    public void setMapValues(List<MapEntity> mapValues) {

    }

    private GridPane createMap() {
        GridPane map = new GridPane();
        short row = 0;
        short col = 0;

        for (int i = 0; i < SIZE_OF_MAP; i++) {
            CoordinatedText textField = createTextField(col, row, String.valueOf(i));
            if(col == COLS) {
                col = 0;
                row++;
            }
            map.add(textField, col++, row);
        }
        return map;
    }

    private GridPane createMapWithValue(int mapValue) {
        GridPane map = new GridPane();
        short row = 0;
        short col = 0;

        for (int i = 0; i < SIZE_OF_MAP; i++) {
            CoordinatedText textField = createTextField(col, row, String.valueOf(mapValue));
            if(col == COLS) {
                col = 0;
                row++;
            }
            map.add(textField, col++, row);
        }
        return map;
    }

    private CoordinatedText createTextField(int x, int y, String text) {

        CoordinatedText coordinatedText = new CoordinatedText(text, x, y);
        coordinatedText.setPrefSize(WIDTH, HEIGHT);
        coordinatedText.setMinSize(WIDTH, HEIGHT);
        coordinatedText.setMaxSize(WIDTH, HEIGHT);
        coordinatedText.setAlignment(Pos.CENTER);
        coordinatedText.setBorder(
                new Border(
                        new BorderStroke(Color.BLACK,
                                BorderStrokeStyle.NONE,
                                null, null)));
        return coordinatedText;
    }

    private CoordinatedText searchByCoordinates(ObservableList<Node> coordinatedTextsList, int x, int y) {
        Optional<Node> node = coordinatedTextsList.stream()
                .filter(t -> {
                    CoordinatedText coordinatedText = (CoordinatedText) t;
                    System.out.println(coordinatedText.getX());
                    System.out.println(coordinatedText.getY());
                    return coordinatedText.getX() == x && coordinatedText.getY() == y;
                }).findFirst();

        if (node.isEmpty()) {
            throw new RuntimeException();
        }

        return (CoordinatedText) node.get();
    }

    private CoordinatedText searchByValue(ObservableList<Node> coordinatedTextsList, String value) {
        Optional<Node> node = coordinatedTextsList.stream()
                .filter(t -> {
                    CoordinatedText coordinatedText = (CoordinatedText) t;
                    return coordinatedText.getText().equals(value);
                }).findFirst();

        if (node.isEmpty()) {
            throw new RuntimeException();
        }

        return (CoordinatedText) node.get();
    }

    private List<CoordinatedText> convertMapValuesToCoordinatedText(List<MapEntity> mapValues) {
        List<CoordinatedText> coordinatedTextsList = new ArrayList<>();
        for (MapEntity mapValue : mapValues) {
            coordinatedTextsList.add(
                    createTextField(mapValue.getX(),
                            mapValue.getY(),
                            String.valueOf(mapValue.getValue())));
        }
        return coordinatedTextsList;
    }
}
