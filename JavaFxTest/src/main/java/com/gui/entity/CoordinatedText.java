package com.gui.entity;

import javafx.scene.control.TextField;

public class CoordinatedText extends TextField {
    private final int x;
    private final int y;

    public CoordinatedText(String value, int x, int y) {
        super(value);
        this.y = y;
        this.x = x;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
