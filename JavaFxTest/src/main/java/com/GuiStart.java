package com;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class GuiStart {
    private GuiStart() {
    }

    public static void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        BorderPane borderPane = fxmlLoader.load(
                Objects.requireNonNull(Application.class.getResource("/views/tableTest.fxml"))
                        .openStream());
        Scene scene = new Scene(borderPane);
        stage.setScene(scene);
        stage.show();
    }
}
