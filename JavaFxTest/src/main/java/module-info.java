module com.javafxtest {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    requires org.controlsfx.controls;
    requires com.dlsc.formsfx;
    requires validatorfx;
    requires eu.hansolo.tilesfx;

    opens com to javafx.fxml;
    opens com.gui.controllers to javafx.fxml;
    exports com;
}