package controllers;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;
import org.opencv.videoio.VideoCapture;
import utils.Utils;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class HomeController {
    @FXML
    private ImageView currentFrame;
    private ScheduledExecutorService timer;
    private final VideoCapture capture = new VideoCapture();

    private final CascadeClassifier faceCascade = new CascadeClassifier();
    private int absoluteFaceSize = 0;

    public void init() {
        capture.open(0);
        if (capture.isOpened()) {
            Runnable frameGrabber = () -> {
                Mat frame = grabFrame();
                Image imageToShow = Utils.mat2Image(frame);
                updateImageView(currentFrame, imageToShow);
            };

            timer = Executors.newSingleThreadScheduledExecutor();
            timer.scheduleAtFixedRate(frameGrabber, 0, 25, TimeUnit.MILLISECONDS);
        } else System.err.println("Impossible to open the camera connection...");
    }

    private Mat grabFrame() {
        Mat frame = new Mat();
        if (capture.isOpened()) {
            try {
                capture.read(frame);
                if (!frame.empty()) detectAndDisplay(frame);
            }
            catch (Exception e) {
                System.err.println("Exception during the image elaboration: " + e);
            }
        }
        return frame;
    }

    private void detectAndDisplay(Mat frame) {
        faceCascade.load("D:\\repos\\practice\\OpenCV-test\\src\\resources\\haarcascades\\haarcascade_frontalface_alt2.xml");
        MatOfRect faces = new MatOfRect();
        Mat grayFrame = new Mat();
        Imgproc.cvtColor(frame, grayFrame, Imgproc.COLOR_BGR2GRAY);
        Imgproc.equalizeHist(grayFrame, grayFrame);
        if (absoluteFaceSize == 0) {
            int height = grayFrame.rows();
            if (Math.round(height * 0.2f) > 0) absoluteFaceSize = Math.round(height * 0.2f);
        }

        faceCascade.detectMultiScale(grayFrame, faces, 1.1, 2, Objdetect.CASCADE_SCALE_IMAGE,
                new Size(absoluteFaceSize, absoluteFaceSize), new Size());

        Rect[] facesArray = faces.toArray();
        for (Rect rect : facesArray) Imgproc.rectangle(frame, rect.tl(), rect.br(), new Scalar(0, 255, 0), 3);
    }

    private void updateImageView(ImageView view, Image image) {
        Utils.onFXThread(view.imageProperty(), image);
    }

    private void stopAcquisition() {
        if (this.timer!=null && !this.timer.isShutdown()) this.timer.shutdown();
        if (this.capture.isOpened()) capture.release();
    }

    public void setClosed() {
        this.stopAcquisition();
    }
}
